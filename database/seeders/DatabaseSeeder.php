<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;

use App\Models\Category;
use App\Models\Post;
use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // User::create([
        //     'name' => 'Jian',
        //     'email' => 'jiannsyah@gmail.com',
        //     'password' => bcrypt('12345')
        // ]);
        // User::create([
        //     'name' => 'Dody',
        //     'email' => 'dody@gmail.com',
        //     'password' => bcrypt('12345')
        // ]);
        User::factory(3)->create();
        Post::factory(10)->create();
        Category::create([
            'name' => 'Web Programming',
            'slug' => 'web-programming'
        ]);
        Category::create([
            'name' => 'Personal',
            'slug' => 'personal'
        ]);
        Category::create([
            'name' => 'Web Design',
            'slug' => 'web-design'
        ]);
        // Post::create([
        //     'title' => 'Judul Pertama',
        //     'slug' => 'judul-pertama',
        //     'excerpt' => 'lorem pertama',
        //     'body' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Quaerat laboriosam hic assumenda eligendi unde fugiat rerum qui necessitatibus, reiciendis consequatur possimus magni accusantium labore corporis quas. Sed officia dolor laudantium tenetur enim architecto amet nihil qui perferendis a error in molestias, cum officiis eligendi repellat veritatis dignissimos ratione doloribus cupiditate cumque harum facilis explicabo. Cumque, atque aut doloribus sequi vero quam ea quod repellendus doloremque velit nam non nostrum odio illum repellat! Reprehenderit minima magnam eaque obcaecati deleniti recusandae perspiciatis, a animi ipsam pariatur unde sint quam, commodi incidunt, omnis perferendis dolores facilis iure soluta facere dicta! Hic, sapiente quis!',
        //     'category_id' => '1',
        //     'user_id' => '1'
        // ]);
        // Post::create([
        //     'title' => 'Judul Kedua',
        //     'slug' => 'judul-kedua',
        //     'excerpt' => 'lorem kedua',
        //     'body' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Quaerat laboriosam hic assumenda eligendi unde fugiat rerum qui necessitatibus, reiciendis consequatur possimus magni accusantium labore corporis quas. Sed officia dolor laudantium tenetur enim architecto amet nihil qui perferendis a error in molestias, cum officiis eligendi repellat veritatis dignissimos ratione doloribus cupiditate cumque harum facilis explicabo. Cumque, atque aut doloribus sequi vero quam ea quod repellendus doloremque velit nam non nostrum odio illum repellat! Reprehenderit minima magnam eaque obcaecati deleniti recusandae perspiciatis, a animi ipsam pariatur unde sint quam, commodi incidunt, omnis perferendis dolores facilis iure soluta facere dicta! Hic, sapiente quis!',
        //     'category_id' => '2',
        //     'user_id' => '2'
        // ]);
        // Post::create([
        //     'title' => 'Judul Ketiga',
        //     'slug' => 'judul-ketiga',
        //     'excerpt' => 'lorem ketiga',
        //     'body' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Quaerat laboriosam hic assumenda eligendi unde fugiat rerum qui necessitatibus, reiciendis consequatur possimus magni accusantium labore corporis quas. Sed officia dolor laudantium tenetur enim architecto amet nihil qui perferendis a error in molestias, cum officiis eligendi repellat veritatis dignissimos ratione doloribus cupiditate cumque harum facilis explicabo. Cumque, atque aut doloribus sequi vero quam ea quod repellendus doloremque velit nam non nostrum odio illum repellat! Reprehenderit minima magnam eaque obcaecati deleniti recusandae perspiciatis, a animi ipsam pariatur unde sint quam, commodi incidunt, omnis perferendis dolores facilis iure soluta facere dicta! Hic, sapiente quis!',
        //     'category_id' => '1',
        //     'user_id' => '1'
        // ]);
        // \App\Models\User::factory(10)->create();

        // \App\Models\User::factory()->create([
        //     'name' => 'Test User',
        //     'email' => 'test@example.com',
        // ]);
    }
}
