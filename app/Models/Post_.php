<?php

namespace App\Models;

class Post
{
    private static $blog_posts = [["title" => "Judul Post Pertama", "slug" => "judul-post-pertama", "author" => "Jian", "body" => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Odit, laboriosam. Unde, molestias soluta pariatur eos, ea sunt quidem minus debitis laudantium ullam sapiente dolorem quasi non totam numquam rerum? Sapiente nihil, fugiat quisquam nostrum sequi nulla repudiandae! Architecto non quam ratione cupiditate quisquam corporis, commodi voluptatibus facere at quis culpa ipsam, quo alias a nam esse rerum voluptates quidem dolore quae quasi asperiores vel possimus! Magnam numquam eum corrupti quia omnis! Perferendis temporibus placeat sapiente corrupti tempora officiis eum voluptatibus!
    '], ["title" => "Judul Post Kedua", "slug" => "judul-post-kedua", "author" => "Amang", "body" => 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Illo sunt dignissimos, eum incidunt libero omnis magnam quis praesentium accusamus, ea nemo soluta voluptatem reiciendis. Ab reiciendis laborum nihil error autem. Fuga deleniti veniam veritatis ipsum dolorem excepturi quidem at, ex asperiores, aliquid dolor amet magnam molestiae error voluptates eos dolores nemo voluptatem cupiditate. Labore voluptatem totam quis dolorum ab fugiat consequuntur alias quibusdam officia, vero recusandae excepturi, quidem magnam fugit accusantium vel quos autem vitae nam tempora amet ut. Repellat enim culpa, cumque nulla, eligendi rem architecto reiciendis quis itaque corrupti harum fuga tenetur blanditiis soluta consequuntur quidem perspiciatis consectetur.
    ']];
    public static function all()
    {
        return collect(self::$blog_posts); //collect merubah  array menjadi bisa digunakan banyak perinta
    }
    public static function find($slug)
    {
        $posts = static::all(); //yang didapat sudah berupa collection
        // $post = [];
        // foreach ($posts as $p) {
        //     if ($p["slug"] === $slug) {
        //         # code...
        //         $post = $p;
        //     }
        // }
        return $posts->firstWhere('slug', $slug);
    }
}
