{{-- @dd($posts); --}}
@extends('layouts.main')
@section('container')
    <h1 class="mb-3 text-center">{{ $title }}</h1>
    <div class="row mb-3 justify-content-center">
        <div class="col-md-6">
            <form action="/posts">
                @if (request('category'))
                    <input type="hidden" name="category" value="{{ request('category') }}">
                @endif
                @if (request('author'))
                    <input type="hidden" name="author" value="{{ request('author') }}">
                @endif
                <div class="input-group mb-3">
                    <input type="text" class="form-control" placeholder="Search.." name="search"
                        value="{{ request('search') }}">
                    <div class="input-group-append">
                        <button class="btn btn-danger" type="submit">Search</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    @if ($posts->count())
        <div class="card mb-3">
            <img class="card-img-top" src="https://source.unsplash.com/1200x400?{{ $posts[0]->category->name }}"
                alt="Card image cap">
            <div class="card-body text-center">
                <h5 class="card-title">
                    <a href="/posts/{{ $posts[0]->slug }}" class="text-dark">
                        {{ $posts[0]->title }}
                    </a>
                </h5>
                <p>
                    <small class="text-muted">
                        By.
                        <a href="/posts?author={{ $posts[0]->author->username }}">
                            {{ $posts[0]->author->name }}
                        </a>
                        {{ $posts[0]->created_at->diffForHumans() }}
                        <a href="/posts?category={{ $posts[0]->category->slug }}">{{ $posts[0]->category->name }}</a>
                    </small>
                </p>
                <p class="card-text">{{ $posts[0]->excerpt }}</p>
                <a href="/posts/{{ $posts[0]->slug }}" class="btn btn-primary">
                    Read more
                </a>
            </div>
        </div>
        <div class="container">
            <div class="row">
                @foreach ($posts->skip(1) as $post)
                    <div class="col-md-4 mb-3">
                        <div class="card">
                            <div class="position-absolute bg-dark px-3 py-2 text-white"
                                style="background-color: rgba(0,0,0,0.1)"><a class="text-white"
                                    href="/posts?category={{ $post->category->slug }}">{{ $post->category->name }}</a>
                            </div>
                            <img class="card-img-top" src="https://source.unsplash.com/500x500?{{ $post->category->name }}"
                                alt="{{ $post->category->name }}">
                            <div class="card-body">
                                <h5 class="card-title">{{ substr($post->title, 1, 25) }}</h5>
                                <p>
                                    <small class="text-muted">
                                        By.
                                        <a href="/posts?author={{ $post->author->username }}">
                                            {{ $post->author->name }}
                                        </a>
                                        {{ $post->created_at->diffForHumans() }}
                                    </small>
                                </p>
                                <p class="card-text">{{ substr($post->excerpt, 1, 100) }}</p>
                                <a href="/posts/{{ $post->slug }}" class="btn btn-primary">Read More...</a>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
        {{-- @foreach ($posts->skip(1) as $post)
        <article class="mb-5">
            <h2>
            </h2>
            <h5>By : {{ $post->author }}</h5>
            <p>{{ $post->excerpt }}</p>
            <a href="/posts/{{ $post->slug }}">
                {{ $post->title }}
            </a>
            <a href="/posts/{{ $post->slug }}"></a>
        </article>
    @endforeach --}}
    @else
        <p class="text-center"> No Post Found!</p>
    @endif
    {{ $posts->links() }}
@endsection
