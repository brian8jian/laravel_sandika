{{-- @dd($post); --}}
@extends('layouts.main')
@section('container')
    <div class="container">
        <div class="row justify-content-center mb-5">
            <div class="col-md-8">
                <h1 class="mb-5">{{ $post->title }}</h1>
                <p>By.
                    <a href="/posts?author={{ $post->author->username }}">
                        {{ $post->author->name }}
                    </a>
                    in
                    <a href="/posts?category={{ $post->category->slug }}">
                        {{ $post->category->name }}
                    </a>
                </p>
                <img class="card-img-top" src="https://source.unsplash.com/1200x400?{{ $post->category->name }}"
                    alt="{{ $post->category->name }}" class="img-fluid">
                {{-- <h5>{{ $post["author"] }}</h5> --}}
                {{-- {{ $post->body }} --}}
                {{-- cara agar terhindar dari special char,tapi harus hati2 --}}
                <article class="my-3 ">
                    {!! $post->body !!}
                </article>

                <a href="/blog" class="d-block mt-3">Back to Blog</a>
            </div>
        </div>
    </div>
@endsection
