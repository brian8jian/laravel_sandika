@extends('dashboard.layouts.main')
@section('container')
    <div class="container">
        <div class="row justify-content-center mb-5">
            <div class="col-md-8">
                <h1 class="mb-5">{{ $post->title }}</h1>
                <p>By.
                    <a href="/posts?author={{ $post->author->username }}">
                        {{ $post->author->name }}
                    </a>
                    in
                    <a href="/posts?category={{ $post->category->slug }}">
                        {{ $post->category->name }}
                    </a>
                </p>
                <a href="/dashboard/posts" class="btn btn-success">
                    <span data-feather="arrow-left"></span>
                    Back to My Posts
                </a>
                <a href="/dashboard/posts/{{ $post->slug }}/edit" class="btn btn-warning">
                    <span data-feather="edit"></span>
                    Edit
                </a>
                <form action="/dashboard/posts/{{ $post->slug }}" method="post" class="d-inline">
                    @method('delete')
                    @csrf
                    <button class="btn btn-danger " onclick="return confirm('Are You sure')">
                        <span data-feather="x-circle"></span>
                        Delete
                    </button>
                </form>
                <img class="card-img-top mt-3" src="https://source.unsplash.com/1200x400?{{ $post->category->name }}"
                    alt="{{ $post->category->name }}" class="img-fluid">
                {{-- <h5>{{ $post["author"] }}</h5> --}}
                {{-- {{ $post->body }} --}}
                {{-- cara agar terhindar dari special char,tapi harus hati2 --}}
                <article class="my-3 ">
                    {!! $post->body !!}
                </article>

            </div>
        </div>
    </div>
@endsection
