 @extends('layouts.main')

 @section('container')
     <h1 class="mb-5">Post Categories</h1>
     <div class="container">
         <div class="row">
             @foreach ($posts as $p)
                 <div class="col-md-4">
                     <div class="card">
                         <a href="/posts?category={{ $p->slug }}" class="text-dark">
                             <div class="position-absolute bg-dark px-3 py-2 text-white"
                                 style="background-color: rgba(0,0,0,0.1)">
                                 <a class="text-white" href="/posts?category={{ $p->slug }}">{{ $p->name }}</a>
                             </div>
                         </a>
                         <img class="card-img-top" src="https://source.unsplash.com/500x500?{{ $p->name }}"
                             alt="{{ $p->name }}">
                     </div>
                 </div>
             @endforeach
         </div>
     </div>
 @endsection
