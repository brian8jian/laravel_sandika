@extends('layouts.main')

@section('container')
    <div class="row justify-content-center">
        <div class="col-md-6">
            <main class="form-registration ">
                <h1 class="h3 mb-3 font-weight-normal text-center">Registration Form</h1>
                <form action="/register" method="POST">
                    @csrf
                    <div class="form-group">
                        <label for="name" class="sr-only">Name</label>
                        <input type="text"name="name" id="name"
                            class="form-control rounded-top @error('name') is-invalid @enderror" placeholder="Name"
                            required />
                        @error('name')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="username" class="sr-only">Username</label>
                        <input type="text"name="username" id="username"
                            class="form-control @error('username') is-invalid @enderror" placeholder="Username" required />
                        @error('username')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="email" class="sr-only">Email</label>
                        <input type="email"name="email" id="email"
                            class="form-control @error('email') is-invalid @enderror" placeholder="name@example.com"
                            required />
                        @error('email')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="password" class="sr-only">Password</label>
                        <input type="password"name="password" id="password"
                            class="form-control rounded-bottom @error('password') is-invalid @enderror"
                            placeholder="Password" required />
                        @error('password')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                    <button class="btn btn-lg btn-primary btn-block mt-3" type="submit">
                        Register
                    </button>
                </form>
                <small class="d-block text-center mt-3">
                    Already Registered? <a href="/login">Login</a>
                </small>
            </main>
        </div>
    </div>
@endsection
